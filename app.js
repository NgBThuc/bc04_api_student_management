const API_URL = "https://62db6ca1e56f6d82a772849e.mockapi.io";

function init() {
  lockEl({ updateStudentBtn, btnShowAll });
  takeStudentList();
}

function takeStudentList() {
  showLoading();
  axios({
    method: "get",
    url: `${API_URL}/sv`,
  })
    .then(function (response) {
      hideLoading();
      renderToHtml(response.data);
    })
    .catch(function (error) {
      hideLoading();
      throw new Error(error);
    });
}

function addStudent() {
  let newStudent = createNewStudent(takeFormValue());
  showLoading();
  axios({
    method: "post",
    url: `${API_URL}/sv`,
    data: newStudent,
  })
    .then(function () {
      resetInput();
      takeStudentList();
    })
    .catch(function (error) {
      throw new Error(error);
    });
}

function deleteStudent(studentId) {
  showLoading();
  axios({
    method: "delete",
    url: `${API_URL}/sv/${studentId}`,
  })
    .then(function () {
      takeStudentList();
      scrollToForm();
    })
    .catch(function (error) {
      throw new Error(error);
    });
}

function adjustStudent(studentId) {
  showLoading();
  axios({
    method: "get",
    url: `${API_URL}/sv/${studentId}`,
  })
    .then(function (response) {
      hideLoading();
      scrollToForm();
      unLockEl({ updateStudentBtn });
      lockEl({ addStudentBtn, resetStudentBtn });
      let student = response.data;
      showUpdateStudentOnForm(student);
    })
    .catch(function (error) {
      hideLoading();
      throw new Error(error);
    });
}

function updateStudent() {
  let student = takeFormValue();
  showLoading();
  axios({
    method: "put",
    url: `${API_URL}/sv/${student.id}`,
    data: student,
  })
    .then(function () {
      takeStudentList();
      resetInput();
    })
    .catch(function (error) {
      throw new Error(error);
    });
}

function searchStudent() {
  let searchValue = takeSearchValue();
  showLoading();
  axios({
    method: "get",
    url: `${API_URL}/sv?name=${searchValue}`,
  })
    .then(function (response) {
      unLockEl({ btnShowAll });
      hideLoading();
      renderToHtml(response.data);
    })
    .catch(function (error) {
      hideLoading();
      throw new Error(error);
    });
}

function resetInput() {
  document.querySelector("#txtMaSV").value = "";
  document.querySelector("#txtTenSV").value = "";
  document.querySelector("#txtEmail").value = "";
  document.querySelector("#txtPass").value = "";
  document.querySelector("#txtDiemToan").value = "";
  document.querySelector("#txtDiemLy").value = "";
  document.querySelector("#txtDiemHoa").value = "";
  unLockEl({ addStudentBtn, resetStudentBtn });
}

function showAllStudent() {
  takeStudentList();
  lockEl({ btnShowAll });
}

init();
