function takeFormValue() {
  const id = document.querySelector("#txtMaSV").value;
  const name = document.querySelector("#txtTenSV").value;
  const email = document.querySelector("#txtEmail").value;
  const password = document.querySelector("#txtPass").value;
  const math = +document.querySelector("#txtDiemToan").value;
  const physics = +document.querySelector("#txtDiemLy").value;
  const chemistry = +document.querySelector("#txtDiemHoa").value;

  return { id, name, email, password, math, physics, chemistry };
}

function takeSearchValue() {
  return document.querySelector("#txtSearch").value;
}

function createNewStudent(studentObj) {
  return new Student(studentObj);
}

function renderToHtml(studentList) {
  let htmlMarkup = "";
  studentList.forEach((studentObj) => {
    let student = createNewStudent(studentObj);
    htmlMarkup += `
    <tr>
      <td id="${student.id}" class="align-middle">${student.id}</td>
      <td class="align-middle">${student.name}</td>
      <td class="align-middle">${student.email}</td>
      <td class="align-middle">${student.average}</td>
      <td class="align-middle">
        <button onclick="deleteStudent(${student.id})" class="btn btn-danger">Delete</button>
        <button onclick="adjustStudent(${student.id})" class="btn btn-warning">Adjust</button>
      </td>
    </tr>`;
  });
  document.querySelector("#tbodySinhVien").innerHTML = htmlMarkup;
}

function showUpdateStudentOnForm(student) {
  document.querySelector("#txtMaSV").value = student.id;
  document.querySelector("#txtTenSV").value = student.name;
  document.querySelector("#txtEmail").value = student.email;
  document.querySelector("#txtPass").value = student.password;
  document.querySelector("#txtDiemToan").value = student.math;
  document.querySelector("#txtDiemLy").value = student.physics;
  document.querySelector("#txtDiemHoa").value = student.chemistry;
}

function showLoading() {
  document.querySelector("#loading").style.display = "grid";
}

function hideLoading() {
  document.querySelector("#loading").style.display = "none";
}

function lockEl(inputIdObj) {
  Object.keys(inputIdObj).forEach((inputId) => {
    document.getElementById(inputId).disabled = true;
  });
}

function unLockEl(inputIdObj) {
  Object.keys(inputIdObj).forEach((inputId) => {
    document.getElementById(inputId).disabled = false;
  });
}

function scrollToForm() {
  window.scrollTo({ top: 0, behavior: "smooth" });
}

function scrollToId(studentId) {
  document.getElementById(studentId).scrollIntoView({
    behavior: "smooth",
  });
}
