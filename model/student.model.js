function Student(student) {
  this.id = student.id;
  this.name = student.name;
  this.email = student.email;
  this.password = student.password;
  this.math = student.math;
  this.physics = student.physics;
  this.chemistry = student.chemistry;

  Object.defineProperty(this, "average", {
    get: function () {
      return ((this.math + this.physics + this.chemistry) / 3).toFixed(2);
    },
  });
}
